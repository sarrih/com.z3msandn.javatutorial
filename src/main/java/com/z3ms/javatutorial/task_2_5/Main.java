package com.z3ms.javatutorial.task_2_5;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class Main {


    /**
     * step 0 - check if not empty
     * step 1 - check if values available
     * step 2 - [1,2,3,..] find elements
     * step 3 - save elements and indexes
     * step 4 - swap.
     */

    public static void main(String[] args) {

        Set<Person> set = new HashSet<>();
        Person p1 = new Person("Reem", "Al Sarrih",1);
        Person p2 = new Person("Ziad", "Al Sarrih",2);
        Person p3 = new Person("Kassem", "Al Sarrih",3);
        Person p4 = new Person("Rayan", "Al Sarrih",4);
        Person p5 = new Person("Ahmad", "Al Sarrih",5);
        Person p6 = new Person("Ra2ouf", "Al Sarrih",6);
        Person p7 = new Person("Kamel", "Al Sarrih",7);
        Person p8 = new Person("Ali", "Al Sarrih",8);
        set.add(p1);
        set.add(p2);
        set.add(p3);
        set.add(p4);
        set.add(p5);
        set.add(p6);
        set.add(p7);
        set.add(p8);
        System.out.println(set);
        Set<Person> set2 = new HashSet<>();
        Person kamel = new Person(getIndexByName(set, "Kamel"),2);
        Person ziad = new Person(getIndexByName(set, "Ziad"),7);

        Iterator<Person> iterator = set.iterator();
        while (iterator.hasNext()) {
            Person next = iterator.next();
            if (next.getName().equals(kamel.getName())) {
                set2.add(ziad);
            }else if (next.getName().equals(ziad.getName())){
                set2.add(kamel);
            }else {
                set2.add(new Person(next,next.getAge()));
            }
        }
        System.out.println(set2);
    }


    private static boolean replace(Set<Person> set, Person toReplace, Person replacement) {
        for (set.iterator(); set.iterator().hasNext(); ) {
            Person next = set.iterator().next();
            if (next.equals(toReplace) ){
                set.remove(next);
                set.add(replacement);
                return true;
            }
        }
        return false;
    }

    private static Person getIndexByName(Set<Person> set, String name) {

        Iterator<Person> iterator = set.iterator();
        while (iterator.hasNext()) {
            Person next = iterator.next();
            if (next.getName().equals(name)) {
                return next;
            }
        }
        return new Person("","",0);
    }
}

